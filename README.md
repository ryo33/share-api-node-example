# share-api-node-example
[sha.herokuapp.com API](https://gist.github.com/ryo33/145e5ef24bad6f11abbb902edc6979d6)

## Installation
- Clone this repository.  
- Change `USER_ID` and `USER_SECRET` of [app.js](app.js).

## Usage
```bash
$ npm start
```
